// Copyright 2023 Enzo Venturi. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
)

var (
	path = flag.String("path", ".", "directory to list")
	long = flag.Bool("long", false, "use long format")
)

func main() {
	flag.Parse()
	ents, err := os.ReadDir(*path)
	if err != nil {
		fmt.Fprintf(os.Stderr, "ls: %s\n", err)
		os.Exit(1)
	}
	for _, e := range ents {
		out := e.Name()
		if *long {
			out = fs.FormatDirEntry(e)
		}
		fmt.Fprintln(flag.CommandLine.Output(), out)
	}
}
