package main

import (
	"bytes"
	"flag"
	"testing"
)

func TestOutput(t *testing.T) {
	var b bytes.Buffer
	flag.CommandLine.SetOutput(&b)
	flag.Set("path", "testdata")

	tests := []struct {
		desc string
		want string
	}{
		{"default", "dir\nfile\n"},
		{"long", "d dir/\n- file\n"},
	}

	for _, tc := range tests {
		t.Run(tc.desc, func(t *testing.T) {
			if tc.desc == "long" {
				flag.Set("long", "true")
			}
			main()
			if b.String() != tc.want {
				t.Errorf("mismatch output: got %+v, want %+v", b.String(), tc.want)
			}
			b.Reset()
		})
	}
}

func BenchmarkDefaultOutput(b *testing.B) {
	flag.Set("path", "testdata")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		main()
	}
}

func BenchmarkLongOutput(b *testing.B) {
	flag.Set("path", "testdata")
	flag.Set("long", "true")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		main()
	}
}
